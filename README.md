# Monday Factory Reporter

## Overview

Reporter is simple extension for sending  user messages from application to support. Reporter is simple extensible and configurable. You can make Your custom templates, processors and service providers.


## Installation

Download via composer

`composer require monday-factory/reporter`

## Minimal configuration

### Register extension

```yaml
extensions:
	reporter: MondayFactory\Reporter\DI\ReporterExtension
```

### Register senders

In this example is used  [reporter sender for Gitlab](https://packagist.org/packages/monday-factory/reporter-sender-gitlab). 

```yaml

reporter:
	senders:
		gitlab:
			class: MondayFactory\ReporterSenderGitlab\Sender\GitlabSender
			config:
				url: https://gitlab.mondayfactory.cz
				token: ***
				username: johndoe
				projectId: 1
```

