<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Sender;

use MondayFactory\Reporter\Helper\ConfigHelper;

abstract class AbstractSender
{

	use ConfigHelper;

}
