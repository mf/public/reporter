<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Sender;

use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

interface ISender
{

	public function send(Form $form, ArrayHash $values);

	public function setConfig(array $config);

}
