<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Helper;

trait ConfigHelper
{

	/**
	 * @var array
	 */
	protected $config;

	/**
	 * @var array
	 */
	protected $minimalConfig = [
	];

	protected function validateConfig()
	{
		$missingConfig = array_diff_key($this->minimalConfig, $this->config);

		if (count($missingConfig) !== 0) {
			throw new \InvalidArgumentException(
				sprintf(
					'Service %s requires config properties [%s], following properties are missing [%s].',
					self::class,
					implode(', ', array_keys($this->minimalConfig)),
					implode(', ', array_keys($missingConfig))
				));
		}
	}

	/**
	 * @param array $config
	 */
	public function setConfig(array $config): void
	{
		$this->config = $config;
		$this->validateConfig();
	}

}
