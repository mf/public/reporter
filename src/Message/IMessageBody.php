<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Message;

interface IMessageBody
{

	public function __toString();

	public function toString(): string;

}
