<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Message;

class MessagePartCollection implements \Countable
{
	/**
	 * @var array
	 */
	private $parts = [];

	public function getParts()
	{
		return $this->parts;
	}

	public function addMessagePart(MessagePart $messagePart): self
	{
		$this->parts[] = $messagePart;

		return $this;
	}

	public function count()
	{
		return count($this->parts);
	}
}
