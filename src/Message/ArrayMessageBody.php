<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Message;

class ArrayMessageBody implements IMessageBody
{

	/**
	 * @var array
	 */
	private $body;

	public function __construct(array $body)
	{
		$this->body = $body;
	}

	public function getBody(): array
	{
		return $this->body;
	}

	public function __toString()
	{
		return implode(',', $this->body);
	}

	public function toString(): string
	{
		return $this->__toString();
	}

}
