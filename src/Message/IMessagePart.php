<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Message;

interface IMessagePart
{

	/**
	 * @param string $title
	 * @param string $body
	 * @param string $link
	 */
	public function __construct(string $title, IMessageBody $body, ?string $link = null);

	/**
	 * @return string
	 */
	public function getTitle(): string;

	/**
	 * @return IMessageBody
	 */
	public function getBody(): IMessageBody;

	/**
	 * @return string
	 */
	public function getLink(): ?string;
}
