<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Message;

class StringMessageBody implements IMessageBody
{

	/**
	 * @var string
	 */
	private $body;

	/**
	 * @param string $body
	 */
	public function __construct(string $body)
	{
		$this->body = $body;
	}

	/**
	 * @return string
	 */
	public function getBody(): string
	{
		return $this->body;
	}

	public function __toString()
	{
		return $this->body;
	}

	public function toString(): string
	{
		return $this->__toString();
	}
}
