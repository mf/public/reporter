<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Message;

use PHP_CodeSniffer\Tests\Core\File\testFECNExtendedClass;

class MessagePart implements IMessagePart
{

	/**
	 * @var string
	 */
	private $title;

	/**
	 * @var IMessageBody
	 */
	private $body;

	/**
	 * @var string
	 */
	private $link;

	/**
	 * @param string $title
	 * @param string $body
	 * @param string $link
	 */
	public function __construct(string $title, IMessageBody $body, ?string $link = null)
	{
		$this->title = $title;
		$this->body = $body;
		$this->link = $link;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @return IMessageBody
	 */
	public function getBody(): IMessageBody
	{
		return $this->body;
	}

	/**
	 * @return string
	 */
	public function getLink(): ?string
	{
		return $this->link;
	}

}
