<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\UI\Renderer;

use MondayFactory\Reporter\Helper\ConfigHelper;

class AbstractRenderer implements IRenderer
{

	use ConfigHelper;

}
