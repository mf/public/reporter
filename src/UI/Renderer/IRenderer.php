<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\UI\Renderer;

interface IRenderer
{

	public function setConfig(array $config);

}
