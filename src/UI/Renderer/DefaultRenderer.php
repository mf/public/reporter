<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\UI\Renderer;

use MondayFactory\Reporter\Form\BugReportForm;
use MondayFactory\Reporter\Helper\ConfigHelper;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Localization\ITranslator;
use Nette\Utils\ArrayHash;

class DefaultRenderer extends Control implements IRenderer
{

	use ConfigHelper;

	/**
	 * @var BugReportForm
	 */
	private $form;

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @param BugReportForm $form
	 * @param ITranslator $translator
	 */
	public function __construct(BugReportForm $form, ?ITranslator $translator = null)
	{
		$this->form = $form;
		$this->translator = $translator;
	}

	public function render(): void
	{
		if ($this->translator instanceof ITranslator) {
			$this->getTemplate()->setTranslator($this->translator);
		}

		$this->getTemplate()->setFile(__DIR__ . '/templates/defaultRenderer.latte');
		$this->getTemplate()->position = isset($this->config['position']) ? $this->config['position'] : 'bottom-left';
		$this->getTemplate()->render();
	}

	/**
	 * @return Form
	 */
	public function createComponentBugReportForm(): Form
	{
		$this->form->onSuccess[] = [$this, 'formSent'];

		return $this->form;
	}

	/**
	 * @param Form $form
	 * @param ArrayHash $values
	 */
	public function formSent(Form $form, ArrayHash $values): void
	{
		$this->form->setValues([
			'subject' => null,
			'message' => null,
		]);
		$this->flashMessage('Zpráva byla odeslána, děkujeme.', 'success');
		$this->redrawControl('form');
		$this->redrawControl('messages');
	}

}
