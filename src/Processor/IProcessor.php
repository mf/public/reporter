<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Processor;

interface IProcessor
{

	public function collectData();

}
