<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Processor;

use MondayFactory\Reporter\Message\ArrayMessageBody;
use MondayFactory\Reporter\Message\MessagePart;
use MondayFactory\Reporter\Message\MessagePartCollection;
use MondayFactory\Reporter\Message\StringMessageBody;
use Nette\Application\Application;
use Nette\Application\UI\Form;
use Nette\Http\Request;
use Nette\Utils\ArrayHash;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

class RequestProcessor
{

	/**
	 * @var Request
	 */
	private $httpRequest;

	/**
	 * @var MessagePartCollection
	 */
	private $messagePartCollection;

	/**
	 * @param Request $httpRequest
	 * @param MessagePartCollection $messagePartCollection
	 */
	public function __construct(MessagePartCollection $messagePartCollection)
	{
		$this->messagePartCollection = $messagePartCollection;
	}

	public function collectData(Form $form, ArrayHash $values)
	{
		foreach ($values['info'] as $name => $info) {
			try {
				$info = Json::decode($info);
			} catch (JsonException $e) {
				$info = $info;
			}

			if (is_array($info)) {
				$this->messagePartCollection->addMessagePart(
					new MessagePart(
						$name,
						new ArrayMessageBody($info)
					)
				);
			} elseif (is_scalar($info)) {
				$this->messagePartCollection->addMessagePart(
					new MessagePart(
						$name,
						new StringMessageBody($info)
					)
				);
			}
		}
	}
}
