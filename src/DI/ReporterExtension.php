<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\DI;

use MondayFactory\Reporter\Form\BugReportForm;
use MondayFactory\Reporter\Message\MessagePartCollection;
use MondayFactory\Reporter\Processor\IProcessor;
use MondayFactory\Reporter\Processor\RequestProcessor;
use MondayFactory\Reporter\Sender\ISender;
use MondayFactory\Reporter\UI\Renderer\DefaultRenderer;
use MondayFactory\Reporter\UI\Renderer\IRenderer;
use MondayFactory\ReporterSenderGitlab\Sender\GitlabSender;
use Nette\DI\CompilerExtension;

class ReporterExtension extends CompilerExtension
{

	/**
	 * @var array
	 */
	private $defaults = [
		'renderer' => NULL,
		'senders' => [],
		'processors' => [],
	];

	/**
	 * @var array
	 */
	protected $config;

	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();
		$this->validateConfig($this->defaults);
	}

	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('form'))
			->setType(BugReportForm::class);

		$this->setProcessors();
		$this->setRenderer();

		$builder->addDefinition($this->prefix('messagePartCollection'))
			->setType(MessagePartCollection::class);

		$builder->addDefinition($this->prefix('processor.request'))
			->setType(RequestProcessor::class);
		$builder->getDefinition($this->prefix('form'))
			->addSetup('$onSuccess[]', [[$builder->getDefinition($this->prefix('processor.request')), 'collectData']]);

		$this->setSenders();
	}

	private function setRenderer(): void
	{
		$builder = $this->getContainerBuilder();

		if (is_array($this->config['renderer']) && array_key_exists('class', $this->config['renderer'])) {
			if (! class_exists($this->config['renderer']['class'])) {
				throw new \InvalidArgumentException(sprintf('Renderer class %s nott found.', $this->config['renderer']['class']));
			} elseif (! in_array(IRenderer::class, class_implements($this->config['renderer']['class']))) {
				throw new \InvalidArgumentException(
					sprintf('Renderer class %s must implement %s interface.', $this->config['renderer']['class'], IRenderer::class)
				);
			}

			$rendererUI = $this->config['renderer']['class'];
		} else {
			$rendererUI = DefaultRenderer::class;
		}

		$builder->addDefinition($this->prefix('renderer'))
			->setType($rendererUI);

		if (isset($this->config['renderer']) && is_array($this->config['renderer'])) {
			unset($this->config['renderer']['class']);

			if (count($this->config['renderer'])) {
				$builder->getDefinition($this->prefix('renderer'))->addSetup('setConfig', [$this->config['renderer']]);
			}
		}
	}

	private function setSenders(): void
	{
		$builder = $this->getContainerBuilder();

		if (! is_array($this->config['senders']) || count($this->config['senders']) === 0) {
			throw new \Exception('You must set minimal one sender.');
		}

		foreach ($this->config['senders'] as $senderKey => $senderMeta) {
			if (array_key_exists('class', $senderMeta)) {
				if (! in_array(ISender::class, class_implements($senderMeta['class']))) {
					throw new \InvalidArgumentException(
						sprintf('Sender must instance of type %s, sender [%s] is instanceof [%s].', ISender::class, $senderKey, implode(',' ,class_implements($senderMeta['class'])))
					);
				}

				$builder->addDefinition($this->prefix('sender' . $senderKey))
					->setType($senderMeta['class']);

				if (array_key_exists('config', $senderMeta)) {
					$builder->getDefinition($this->prefix('sender' . $senderKey))
						->addSetup('setConfig', [$senderMeta['config']]);
				}

				$builder->getDefinition($this->prefix('form'))
					->addSetup('$onSuccess[]', [[$builder->getDefinition($this->prefix('sender' . $senderKey)), 'send']]);
			}
		}
	}

	private function setProcessors()
	{
		$builder = $this->getContainerBuilder();

		if (array_key_exists('processors', $this->config) && is_array($this->config['processors'])) {
			foreach ($this->config['processors'] as $processorKey => $processor) {
				if (class_exists($processor)) {
					if (! in_array(IProcessor::class, class_implements($processor))) {
						throw new \InvalidArgumentException(sprintf('Processor [%s] not implement interface %s.', $processorKey, IProcessor::class));
					}

					$builder->addDefinition($this->prefix('processor.' . $processorKey))
						->setType($processor)
						->addTag('reporter.processor');

					$builder->getDefinition($this->prefix('form'))
						->addSetup('$onSuccess[]', [[$builder->getDefinition($this->prefix('processor.' . $processorKey)), 'collectData']]);
				} else {
					throw new \InvalidArgumentException(sprintf('Reporter processor class %s  of processor [%s] not found.', $processor, $processorKey));
				}
			}
		}
	}



}
