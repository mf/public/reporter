<?php

declare(strict_types=1);

namespace MondayFactory\Reporter\Form;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use Nette\Http\Request;
use Nette\Localization\ITranslator;
use Nette\Utils\ArrayHash;
use Nette\Utils\Json;

class BugReportForm extends Form
{

	/**
	 * @var ITranslator
	 */
	private $translator;


	/**
	 * @param IContainer|null $parent
	 * @param string|null $name
	 * @param Request $request
	 * @param ITranslator|null $translator
	 *
	 * @throws \Nette\Utils\JsonException
	 */
	public function __construct(IContainer $parent = null, string $name = null, Request $request, ?ITranslator $translator = null)
	{
		if ($this->translator instanceof ITranslator) {
			$this->setTranslator($this->translator);
		}

		$this->addText('subject', 'Předmět')
			->setRequired();
		$this->addTextArea('message', 'Chyba');
		$this->addMultiUpload('attachments');

		$info = $this->addContainer('info');
		$info->addHidden('url', Json::encode($request->getUrl()));
		$info->addHidden('method', Json::encode($request->getMethod()));
		$info->addHidden('post', Json::encode($request->getPost()));

		$this->addSubmit('send', 'Nahlásit');
	}
}
